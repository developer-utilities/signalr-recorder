﻿using System;
using Interceptor.Providers;
using Interceptor.Proxy;
using Interceptor.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Interceptor
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSingleton<ISignalRUrlProvider, SignalRUrlProvider>();
            services.AddSingleton<ISignalRProxy, SignalRProxy>();

            var useDummyData = Convert.ToBoolean(Configuration["useDummyData"]);

            if (useDummyData)
            {
                services.AddSingleton<IObjectMessageService, StubObjectMessageService>();
            }
            else
            {
                services.AddSingleton<IObjectMessageService, ObjectMessageService>();
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}