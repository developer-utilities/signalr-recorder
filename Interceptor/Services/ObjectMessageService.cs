﻿using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace Interceptor.Services
{
    public class ObjectMessageService : IObjectMessageService
    {
        private readonly ILogger<ObjectMessageService> _logger;

        // TODO: this will be replaced with a much more sophistacted service. This is just a stepping stone.
        private readonly List<object> _messages = new List<object>();

        public ObjectMessageService(ILogger<ObjectMessageService> logger)
        {
            _logger = logger;
        }

        public void Put(object message)
        {
            _logger.LogInformation($"At index [{_messages.Count}], putting message: {message}");

            _messages.Add(message);
        }

        public object Get(int index)
        {
            _logger.LogInformation($"Getting message at index [{index}]");
            return _messages[index];
        }

        public bool Clear()
        {
            _messages.Clear();
            return true;
        }

        public int Size()
        {
            return _messages.Count;
        }

        public List<object> GetAll()
        {
            return _messages;
        }
    }
}