﻿using System.Collections.Generic;
using Microsoft.Extensions.Logging;

namespace Interceptor.Services
{
    public class StubObjectMessageService : IObjectMessageService
    {
        private readonly ILogger<StubObjectMessageService> _logger;
        private readonly List<object> _messages = new List<object>();

        public StubObjectMessageService(ILogger<StubObjectMessageService> logger)
        {
            _logger = logger;

            _logger.LogInformation("Using stub message service");
            _messages.Add(new SampleDto
            {
                Id = 78,
                Name = "Daredevil"
            });

            _messages.Add(new SampleDto
            {
                Id = 53, Name = "Bullseye"
            });
        }

        public void Put(object message)
        {
            _logger.LogInformation($"At index [{_messages.Count}], putting message: {message}");

            _messages.Add(message);
        }

        public object Get(int index)
        {
            _logger.LogInformation($"Getting message at index [{index}]");
            return _messages[index];
        }

        public bool Clear()
        {
            _messages.Clear();
            return true;
        }

        public int Size()
        {
            return _messages.Count;
        }

        public List<object> GetAll()
        {
            return _messages;
        }
    }

    internal class SampleDto
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}