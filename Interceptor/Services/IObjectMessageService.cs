﻿using System.Collections.Generic;

namespace Interceptor.Services
{
    public interface IObjectMessageService
    {
        void Put(object message);
        object Get(int index);
        bool Clear();
        int Size();
        List<object> GetAll();
    }
}