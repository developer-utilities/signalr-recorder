﻿using Microsoft.Extensions.Configuration;

namespace Interceptor.Providers
{
    public class SignalRUrlProvider : ISignalRUrlProvider
    {
        private readonly string _url;

        public SignalRUrlProvider(IConfiguration config)
        {
            // TODO: URL needs to be fully qualified url...
            _url = config["url"] ?? "hubs/V1/DisplayContent";
        }

        public string GetUrl()
        {
            return _url;
        }
    }
}