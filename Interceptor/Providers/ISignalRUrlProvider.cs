﻿namespace Interceptor.Providers
{
    public interface ISignalRUrlProvider
    {
        string GetUrl();
    }
}