﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Interceptor.Providers;
using Interceptor.Services;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Interceptor.Proxy
{
    public class SignalRProxy : ISignalRProxy
    {
        private readonly ILogger<SignalRProxy> _logger;
        private readonly IObjectMessageService _objectMessageService;

        private readonly List<string> _recordedMethods;

        private readonly string _url;
        private HubConnection _connection;

        public SignalRProxy(ILogger<SignalRProxy> logger,
            IConfiguration config,
            ISignalRUrlProvider urlProvider,
            IObjectMessageService objectMessageService)
        {
            _logger = logger;
            _objectMessageService = objectMessageService;
            _url = urlProvider.GetUrl();

            _recordedMethods = config.GetSection("recordedMethods").Get<List<string>>();
        }

        public async Task<bool> Connect()
        {
            _logger.LogDebug("Connecting...");

            if (IsConnected())
            {
                _logger.LogDebug("Connection already established. Disconnecting and reconnecting...");
                await Disconnect();
            }

            try
            {
                BuildHubConnection();
                SetupListeners();

                await StartConnection();
            }
            catch (Exception)
            {
                _logger.LogError("Exception throw when attempting to establish initial SignalR connection.");
                return false;
            }

            return true;
        }

        public async Task<bool> Disconnect()
        {
            _logger.LogDebug("Disconnecting...");
            await _connection.StopAsync();
            return true;
        }

        public async void Invoke(string methodName,
            object[] args,
            CancellationToken cancellationToken = default(CancellationToken))
        {
            _logger.LogDebug($"Invoking [{methodName}]");

            await Connect();

            // Limitation: currently only handles single parameters SignalR methods
            await _connection.InvokeAsync<object>(methodName, args[0], cancellationToken);
        }

        private void SetupListeners()
        {
            _logger.LogDebug("Seting up listeners...");

            foreach (var method in _recordedMethods)
            {
                _logger.LogInformation($"Setting up listener for method: [{method}]");

                _connection.On<object>(method, message =>
                {
                    _logger.LogInformation($"[{method}] message: {message}");
                    _objectMessageService.Put(message);
                });
            }
        }

        private bool IsConnected()
        {
            var isConnected = _connection != null;
            _logger.LogDebug($"IsConnected? [{isConnected}]");
            return isConnected;
        }

        private void AddHandlerForClosedConnection()
        {
            _logger.LogDebug("Adding handler for closed connection...");
            _connection.Closed += async error =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                _logger.LogInformation("Connection lost, attempting reconnect.");
                await _connection.StartAsync();
            };
        }

        private async Task StartConnection()
        {
            _logger.LogDebug("Starting connection...");
            await _connection.StartAsync();
        }

        private void BuildHubConnection()
        {
            _logger.LogDebug("Building hub connection...");

            _connection = new HubConnectionBuilder()
                .WithUrl(_url)
                .Build();
        }
    }
}