﻿using System.Threading;
using System.Threading.Tasks;

namespace Interceptor.Proxy
{
    public interface ISignalRProxy
    {
        Task<bool> Connect();

        Task<bool> Disconnect();

        void Invoke(string methodName,
            object[] args,
            CancellationToken cancellationToken = default(CancellationToken));

    }
}