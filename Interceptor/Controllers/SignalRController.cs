﻿using System.Collections.Generic;
using System.Text;
using Interceptor.Proxy;
using Interceptor.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Interceptor.Controllers
{
    [Route("api/signalr")]
    [ApiController]
    public class SignalRController : ControllerBase
    {
        private readonly JObject _config;
        private readonly ILogger<SignalRController> _logger;
        private readonly IObjectMessageService _objectMessageService;
        private readonly ISignalRProxy _proxy;

        public SignalRController(ILogger<SignalRController> logger,
            ISignalRProxy proxy,
            IObjectMessageService objectMessageService,
            IConfiguration configuration)
        {
            _logger = logger;
            _proxy = proxy;
            _objectMessageService = objectMessageService;

            var list = new List<string>();
            configuration.GetSection("recordedMethods").Bind(list);

            var sb = new StringBuilder();
            foreach (var method in list)
            {
                sb.Append(method + " ");
            }

            _config = new JObject
            {
                {"url", configuration["url"]},
                {"useDummyData", configuration["useDummyData"]},
                {"recordedMethods", sb.ToString()}
            };

            _logger.LogInformation("\n\nConfiguration settings: " + JsonConvert.SerializeObject(_config) + "\n");
        }

        [HttpGet("config")]
        public ActionResult<JObject> GetConfig()
        {
            _logger.LogInformation("Getting config...");

            return Ok(_config);
        }

        [HttpGet("connect")]
        public ActionResult<bool> Connect()
        {
            _logger.LogInformation("Attempting to connect...");
            return _proxy.Connect().Result;
        }

        [HttpGet("disconnect")]
        public ActionResult<bool> Disconnect()
        {
            _logger.LogInformation("Attempting to disconnect...");
            return _proxy.Disconnect().Result;
        }

        [HttpPost("invoke/{methodName}")]
        public void Invoke([FromRoute] string methodName,
            [FromBody] List<object> args)
        {
            _logger.LogInformation($"Attempt to invoke method [{methodName}]");

            _proxy.Invoke(methodName, args.ToArray());
        }

        [HttpGet("messages/{index}")]
        public ActionResult<object> GetMessage([FromRoute] int index)
        {
            _logger.LogInformation($"Getting index from store: [{index}]. Current store size: [{_objectMessageService.Size()}]");
            return _objectMessageService.Get(index);
        }

        [HttpGet("messages")]
        public ActionResult<List<object>> GetAll()
        {
            _logger.LogInformation("Getting all messages from data store");
            return _objectMessageService.GetAll();
        }

        [HttpGet("clear")]
        public ActionResult<bool> ClearDataStore()
        {
            _logger.LogInformation($"Size before clear: [{_objectMessageService.Size()}]");
            var result = _objectMessageService.Clear();
            _logger.LogInformation($"Size after clear: [{_objectMessageService.Size()}]");
            return result;
        }
    }
}